<?php

/**
 * @file
 * Defines migrations for this module.
 */

/**
 * Implements hook_migrate_api().
 *
 * @see https://www.drupal.org/node/1824884
 */
function webfocus_import_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'webfocus_import_group' => array(
        'title' => t('HFC WebFOCUS Import'),
      ),
    ),
    'migrations' => array(
      'CoursesAcsFees' => array(
        'class_name' => 'CoursesAcsFeesMigration',
        'group_name' => 'webfocus_import_group',
      ),
    ),
  );
  return $api;
}
