<?php

/**
 * @file
 * Contains mappings for the Course ACS Fees migrations.
 *
 * @see https://www.drupal.org/node/1006984
 */

class CoursesAcsFeesMigration extends Migration {

  /**
   * Initialize the migration object.
   *
   * @see Migration::__construct()
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // $query = Database::getConnection('default', 'webfocus_import')
    //   ->select('COURSES_ACS_FEES')
    $query = db_select('COURSES_ACS_FEES')
      ->fields('COURSES_ACS_FEES', array('COURSES_ID', 'CRS_NAME', 'CRS_TITLE', 'CRS_SHORT_TITLE', 'CAPACITY', 'ACS_CODE', 'WAITLIST', 'CRSFEE', 'LABFEE', 'MISCFEE'));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('supplemental_course_info');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'COURSES_ID' => array('type' => 'int',
            'unsigned' => FALSE,
            'not null' => FALSE,
            'description' => 'Courses ID',
           ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'CRS_TITLE'); // This doesn't matter because automatic_nodetitle.
    $this->addFieldMapping('field_course_master', 'CRS_NAME')->callbacks(array($this, 'mapCourseMaster'))->defaultValue('');

    global $user;
    // $this->addFieldMapping('uid')->defaultValue($user->uid);
    // $this->addFieldMapping('revision_uid')->defaultValue($user->uid);

    $this->addFieldMapping('status')->defaultValue(1);
    // $this->addFieldMapping('created')->defaultValue(REQUEST_TIME);
    // $this->addFieldMapping('changed')->defaultValue(REQUEST_TIME);
    $this->addFieldMapping('promote')->defaultValue(1);
    $this->addFieldMapping('sticky')->defaultValue(0);
    $this->addFieldMapping('comment')->defaultValue(0);
    $this->addFieldMapping('language')->defaultValue(LANGUAGE_NONE);
    $this->addFieldMapping('revision')->defaultValue(0);
    $this->addFieldMapping('path')->defaultValue('');
    $this->addFieldMapping('log')->defaultValue('');

    $this->addFieldMapping('field_crs_short_title', 'CRS_SHORT_TITLE')->defaultValue('');
    $this->addFieldMapping('field_crs_short_title:language')->defaultValue(LANGUAGE_NONE);
    $this->addFieldMapping('field_crs_acscode', 'ACS_CODE')->defaultValue('');
    $this->addFieldMapping('field_crs_acscode:language')->defaultValue(LANGUAGE_NONE);

    $this->addFieldMapping('field_crs_capacity', 'CAPACITY')->defaultValue('');
    $this->addFieldMapping('field_crs_waitlist', 'WAITLIST')->callbacks(array($this, 'mapWaitlistFlag'))->defaultValue(0);
    $this->addFieldMapping('field_crs_course_fee', 'CRSFEE')->defaultValue('');
    $this->addFieldMapping('field_crs_lab_fee', 'LABFEE')->defaultValue('');
    $this->addFieldMapping('field_crs_misc_dept_fee', 'MISCFEE')->defaultValue('');
    $this->addFieldMapping('field_hank_courses_id', 'COURSES_ID');

    // I cannot figure out how to get rid of the node counters.
    // Setting them DNM (do not map) does not do it.
    // Just truncate node_count in the database after the migration
    // to get rid of these.
    // $this->addFieldMapping('totalcount')->defaultValue(NULL);
    // $this->addFieldMapping('daycount')->defaultValue(NULL);
    // $this->addFieldMapping('timestamp')->defaultValue(NULL);

  }

  /**
   * Implements prepareRow(). This method is called from the source
   * plugin upon first pulling the raw data from the source.
   *
   * @param $row
   *  Object containing raw source data.
   * @return bool
   *  TRUE to process this row, FALSE to have the source skip it.
   *
   * @see https://www.drupal.org/node/1132582
   */
  public function prepareRow($row) {
    // Always include this fragment at the beginning of every prepareRow()
    // implementation, so parent classes can ignore rows.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if (empty($row->CRS_NAME)) {
      // Generate a notice and skip this row if CRS_NAME is empty.
      $this->queueMessage(t('CRS_NAME is empty for record @courses_id', array('@courses_id' => $row->COURSES_ID)), MigrationBase::MESSAGE_NOTICE);
      return FALSE;
    }
    elseif (! $course_master = $this->mapCourseMaster($row->CRS_NAME)) {
      // Generate a notice and skip this row if CRS_NAME does not map to a Course Master.
      $this->queueMessage(t('Could not find Course Master for @crs_name', array('@crs_name' => $row->CRS_NAME)), MigrationBase::MESSAGE_NOTICE);
      return FALSE;
    }

    return TRUE;

  }

  /**
   * Map Waitlist values from N/Y to 0/1.
   *
   * @param string $flag
   *   The Y/N value from HANK.
   * @return int
   *   Returns a 0/1 value for boolean field.
   */
  protected function mapWaitlistFlag($flag) {
    $mapping = array('N' => 0, 'Y' => 1);
    if (!empty($flag) && !empty($mapping[$flag])) {
      return $mapping[$flag];
    }
  }

  /**
   * Map HANK CRS_NAME to Course Master Entity Reference target nid.
   *
   * @param string $crs_name
   *   The HANK Course Name SUBJ-NUMBER
   * @return int
   *   The Node ID of the corresponding Course Master, if found.
   */
  protected function mapCourseMaster($crs_name) {
    if (!empty($crs_name)) {
      list($subject, $number) = explode('-', $crs_name);

      $query = new EntityFieldQuery();
      $result = $query
        ->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'course_master')
        ->fieldCondition('field_crs_subj', 'value', $subject)
        ->fieldCondition('field_crs_num', 'value', $number)
        ->execute();
      if (!empty($result['node'])) {
        $nids = array_keys($result['node']);
        return reset($nids);
      }
      else {
        return FALSE;
      }
    }
  }
}
